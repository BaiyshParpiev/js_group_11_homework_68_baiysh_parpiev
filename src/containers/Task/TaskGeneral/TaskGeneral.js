
import Task from "../AddTask/AddTask";
import {useDispatch, useSelector} from "react-redux";
import {addTask, changeInput, changeTask, deleteTask, fetchTask, saveTask} from "../../../store/actionsForTask";
import {useEffect} from "react";

const TaskGeneral = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchTask())
    }, [dispatch])

    const addNewTask = e => {
        e.preventDefault();
        dispatch(addTask(state.input))
        dispatch(saveTask());
    };

    const onChangeStatus = (id) => {
        dispatch(changeTask(id))
        dispatch(saveTask());
    }

    const removeTask = id => {
        dispatch(deleteTask(id))
        dispatch(saveTask());
    };

    const inputText = e => {
        dispatch(changeInput(e.value));
    }


    const taskComponents = state.task.map(t => (
        <Task
            key={t.id}
            className={t.className}
            number={t.number}
            text={t.text}
            onRemove={() => removeTask(t.id)}
            onChangeStatus={() => onChangeStatus(t.id)}
        >
        </Task>
    ));


    return (
        <div className="container">
            <div className="form__group field">
                <input type="input" className="form__field" placeholder="Name" name="name" id='name'
                       onChange={e => inputText(e.target)} value={state.input} required/>
                <label htmlFor="name" className="form__label">Enter your task</label>
                <button className='addTask' onClick={addNewTask}>Add task</button>
            </div>
            <div className="task-list">{taskComponents}</div>
        </div>
    )
};

export default TaskGeneral;
