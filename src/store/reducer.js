import {
    ADD,
    DECREASE,
    FETCH_COUNTER_FAILURE,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREASE, SAVE_COUNTER_FAILURE, SAVE_COUNTER_REQUEST, SAVE_COUNTER_SUCCESS,
    SUBTRACT
} from "./actions";
import {nanoid} from 'nanoid'
import {
    ADD_TASK,
    CHANGE_INPUT,
    CHANGE_TASK,
    DELETE_TASK, FETCH_TASK_FAILURE,
    FETCH_TASK_REQUEST,
    FETCH_TASK_SUCCESS, SAVE_TASK_FAILURE, SAVE_TASK_REQUEST, SAVE_TASK_SUCCESS
} from "./actionsForTask";

const initialState = {
    counter: 123,
    input: '',
    task: [],
    loading: false,
    error: null,
};

const reducer = (state = initialState, action) => {
    // if(action.type === INCREASE){
    //     return {...state, counter : state.counter + 1}
    // }
    // if(action.type === ADD){
    //     return {...state, counter : state.counter + action.payload}
    // }
    //
    // if(action.type === DECREASE){
    //     return {...state, counter: state.counter - 1}
    // }
    // if(action.type === SUBTRACT){
    //     return {...state, counter: state.counter - action.payload}
    // }
    // return state;

    const add = (state, action) => {
        return {...state, counter: state.counter + action.payload}
    }


    const change = (action, state) => {
        return state.task.map(p => {
            if (p.id === action.payload) {
                return {...p, className: 'task task-done'};
            }

            return p;
        });
    }

    const remove = (action, state) => {
        return state.task.filter(p => p.id !== action.payload).map(t =>{
            const n = state.task.indexOf(t)
            if(t.number === n + 1){
                return {...t, number: t.number}
            }
            return {...t, number: t.number - 1}
        });
    }

    switch (action.type) {
        case INCREASE:
            return {...state, counter: state.counter + 1};
        case DECREASE:
            return {...state, counter: state.counter - 1};
        case ADD:
            return add(state, action);
        case SUBTRACT :
            return {...state, counter: state.counter - action.payload};
        case FETCH_COUNTER_REQUEST :
            return {...state, loading: true, error: null};
        case FETCH_COUNTER_SUCCESS:
            return {...state, loading: false, counter: action.payload};
        case FETCH_COUNTER_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SAVE_COUNTER_REQUEST :
            return {...state, loading: true, error: null};
        case SAVE_COUNTER_SUCCESS:
            return {...state, loading: false, counter: action.payload};
        case SAVE_COUNTER_FAILURE:
            return {...state, loading: false, error: action.payload};
        case ADD_TASK :
            return {...state, task: [...state.task, {id: nanoid(), number: state.task.length + 1, className: 'task', text: action.payload}], input: ''};
        case CHANGE_TASK:
            return {...state, task: change(action, state)};
        case CHANGE_INPUT:
            return {...state, input: action.payload};
        case DELETE_TASK:
            return {...state, task: remove(action, state)};
        case FETCH_TASK_REQUEST :
            return {...state, loading: true, error: null};
        case FETCH_TASK_SUCCESS:
            return {...state, loading: false, task: action.payload};
        case FETCH_TASK_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SAVE_TASK_REQUEST :
            return {...state, loading: true, error: null};
        case SAVE_TASK_SUCCESS:
            return {...state, loading: false, counter: action.payload};
        case SAVE_TASK_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}

export default reducer;