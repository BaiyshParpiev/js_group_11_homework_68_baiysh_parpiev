import axios from 'axios';


export const ADD_TASK = 'ADD_TASK';
export const CHANGE_TASK = 'CHANGE_TASK';
export const CHANGE_INPUT = 'CHANGE_INPUT';
export const DELETE_TASK = 'DELETE_TASK';


export const FETCH_TASK_REQUEST = 'FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
export const FETCH_TASK_FAILURE = 'FETCH_TASK_FAILURE';

export const SAVE_TASK_REQUEST = 'SAVE_TASK_REQUEST';
export const SAVE_TASK_SUCCESS = 'SAVE_TASK_SUCCESS';
export const SAVE_TASK_FAILURE = 'SAVE_TASK_FAILURE';

export const addTask = value => ({type : ADD_TASK, payload: value});
export const changeTask = id => ({type: CHANGE_TASK, payload: id});
export const changeInput = value => ({type: CHANGE_INPUT, payload: value});
export const deleteTask = id => ({type: DELETE_TASK, payload: id});


export const fetchTaskRequest = () => ({type: FETCH_TASK_REQUEST});
export const fetchTaskSuccess = task => ({type: FETCH_TASK_SUCCESS, payload : task});
export const fetchTaskFailure = () => ({type: FETCH_TASK_FAILURE});

export const saveTaskRequest = () => ({type: SAVE_TASK_REQUEST});
export const saveTaskSuccess = task => ({type: SAVE_TASK_SUCCESS, payload : task});
export const saveTaskFailure = () => ({type: SAVE_TASK_FAILURE});


export const fetchTask= () => {
    return async (dispatch) => {
        dispatch(fetchTaskRequest())
        try{
            const response = await axios.get('https://baiysh-parpiev-default-rtdb.firebaseio.com/task.json');
            if(response.data === null){
                dispatch(fetchTaskSuccess(''))
            }else{
                dispatch(fetchTaskSuccess(response.data))
            }
        }catch(e){
            dispatch(fetchTaskFailure());
        }
    }
}

export const saveTask = () => {
    return async (dispatch, getState) => {
        const currentTask = getState().task;
        dispatch(saveTaskRequest())
        try{
            await axios.put('https://baiysh-parpiev-default-rtdb.firebaseio.com/task.json', currentTask);
            dispatch(saveTaskSuccess(currentTask));
        }catch(e){
            dispatch(saveTaskFailure());
        }
    }
}