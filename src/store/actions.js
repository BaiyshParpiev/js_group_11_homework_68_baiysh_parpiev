import axios from "axios";

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';


export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER_REQUEST = 'SAVE_COUNTER_REQUEST';
export const SAVE_COUNTER_SUCCESS = 'SAVE_COUNTER_SUCCESS';
export const SAVE_COUNTER_FAILURE = 'SAVE_COUNTER_FAILURE';


export const increase = () => ({type : INCREASE});
export const add = value => ({type:  ADD, payload: value});
export const decrease = () => ({type: DECREASE});
export const subtract = value => ({type: SUBTRACT, payload: value});


export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload : counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const saveCounterRequest = () => ({type: SAVE_COUNTER_REQUEST});
export const saveCounterSuccess = counter => ({type: SAVE_COUNTER_SUCCESS, payload : counter});
export const saveCounterFailure = () => ({type: SAVE_COUNTER_FAILURE});



export const fetchCount= () => {
    return async (dispatch) => {
        dispatch(fetchCounterRequest())
        try{
            const response = await axios.get('https://baiysh-parpiev-default-rtdb.firebaseio.com/counter.json');
            if(response.data === null){
                dispatch(fetchCounterSuccess(0))
            }else{
                dispatch(fetchCounterSuccess(response.data))
            }
        }catch(e){
            dispatch(fetchCounterFailure());
        }
    }
}

export const saveCounter = () => {
    return async (dispatch, getState) => {
        const currentCounter = getState().counter;
        dispatch(saveCounterRequest())
        try{
            await axios.put('https://baiysh-parpiev-default-rtdb.firebaseio.com/counter.json', currentCounter);
            dispatch(saveCounterSuccess(currentCounter));
        }catch(e){
            dispatch(saveCounterFailure());
        }
    }
}