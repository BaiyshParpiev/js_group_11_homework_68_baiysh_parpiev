import Counter from "./containers/Counter/Counter";
import TaskGeneral from "./containers/Task/TaskGeneral/TaskGeneral";
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import './store/Nav.css';

const App = () => {
    return (
        <BrowserRouter>
            <nav>
                <NavLink to='/counter'> Counter</NavLink>
                <NavLink to='/task'>Task</NavLink>
            </nav>
            <Switch>
                <Route path='/counter' component={Counter}/>
                <Route path='/task' component={TaskGeneral}/>
            </Switch>
        </BrowserRouter>
    )
};

export default App;
